import React from 'react';

import './Wizard.css';

import {Model} from'./Model.js';
import Question from './Question.js';

class Wizard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model: new Model().getModel(),
            currentStep: 0
        }
    }

    onNextControlClick = (answeredQuestion) => {
        this.setState(prevState => {
            return {
                currentStep: prevState.currentStep + 1
            }
        });
    }

    onFinishButtonClick = () => {
        alert('Quiz finished');
        console.log(this.state.model);
    }

    render() {
        return (
            <div className="wizard">

                Current question: {this.state.currentStep + 1}
                <Question
                    question={this.state.model.questions[this.state.currentStep]}
                    stepIndex={this.state.currentStep}
                    stepCount={this.state.model.questions.length}
                    onNextControlClick={this.onNextControlClick}
                    onFinishButtonClick={this.onFinishButtonClick} />
            </div>
        );
    }
}

export default Wizard;
