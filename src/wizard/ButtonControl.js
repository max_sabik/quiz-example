import React from 'react';

class ButtonControl extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="button-control">
                <button onClick={this.props.onClick}>
                    {this.props.text}
                </button>
            </div>
        );
    }
}

export default ButtonControl;
