import React from 'react';

import './Answers.css';

class Answers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            answers: this.props.answers,
            type: this.props.type
        }
    }

    componentWillReceiveProps(props) {
        this.setState({
            answers: props.answers
        });
    }

    onAnswerClick = (chosenAnswerIndex) => {
        this.setState(prevState => {
            let answers = Object.assign([], prevState.answers);

            if(this.state.type === 'SINGLE_RESULT') {
                answers.forEach(answer => {
                    answer.chosen = false;
                });
            }

            answers[chosenAnswerIndex].chosen = true;

            return {
                answers: answers
            };
        });
    }

    render() {
        return (this.state.answers.map((answer, i) => {
            return (
                <div className="answers" key={i}>
                    <input
                        id={"answer-checkbox-control-" + i}
                        type="checkbox"
                        value={this.state.answers[i].text}
                        onChange={() => this.onAnswerClick(i)}
                        checked={this.state.answers[i].chosen} />
                    <label htmlFor={"answer-checkbox-control-" + i}>{this.state.answers[i].text}</label>
                </div>)
        }))
    }
}

export default Answers;
