export class Model {
    constructor() {
        this.wizardModel =
        {
           "userEmail": "maxim.sabarnya@gmail.com",
           "result": 0.8,
           "type": "ONE_WAY_DIRECTION",
           "questions": [
              {
                 "text": "Question 1",
                 "type": "SINGLE_RESULT",
                 "answers": [
                    {
                       "text": "Answer 1.1",
                       "correct": true
                    },
                    {
                       "text": "Answer 1.2",
                       "correct": false
                    },
                    {
                       "text": "Answer 1.3",
                       "correct": false
                    }
                 ]
              },
              {
                 "text": "Question 2",
                 "type": "SINGLE_RESULT",
                 "answers": [
                    {
                       "text": "Answer 2.1",
                       "correct": false
                    },
                    {
                       "text": "Answer 2.2",
                       "correct": true
                    },
                    {
                       "text": "Answer 2.3",
                       "correct": false
                    }
                 ]
              },
              {
                   "text": "Question 3",
                   "type": "MULTIPLE_RESULT",
                   "answers": [
                      {
                         "text": "Answer 3.1",
                         "correct": false
                      },
                      {
                         "text": "Answer 3.2",
                         "correct": true
                      },
                      {
                         "text": "Answer 3.3",
                         "correct": false
                      }
                   ]
                }
           ]
        };
    }

    getModel() {
        return this.wizardModel;
    }
}
