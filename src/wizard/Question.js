import React from 'react';

import './Question.css';

import Answers from './Answers';
import ButtonControl from './ButtonControl';

class Question extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="question">
                <div className="question-text">
                    {this.props.question.text}
                </div>
                <Answers
                    answers={this.props.question.answers}
                    type={this.props.question.type}/>

                    {
                        this.props.stepCount === this.props.stepIndex + 1
                            ? <ButtonControl
                                text="Finish"
                                onClick={this.props.onFinishButtonClick} />
                            : <ButtonControl
                                text="Next"
                                onClick={this.props.onNextControlClick} />
                    }
            </div>
        );
    }
}

export default Question;
