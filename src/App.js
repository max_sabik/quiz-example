import React from 'react';
import './App.css';
import Wizard from './wizard/Wizard';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        WIZARD SIMPLE TEST APP
      </header>
      <div className="wizard-body">
        <Wizard />
      </div>
    </div>
  );
}

export default App;
